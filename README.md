
# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Jose Luis García Fernández
* Titulación: Doble Grado Ingeniería en Sistemas de Telecomunicación y Administración de Empresas
* Cuenta en laboratorios: jlgf
* Cuenta URJC: jl.garciaf.2017
* Video básico (url): https://www.youtube.com/watch?v=5Oo7hEGobQU
* Video parte opcional (url): https://www.youtube.com/watch?v=43PC1jf5x78
* Despliegue (url): http://chaos20.pythonanywhere.com
* Contraseñas: km21, calvos
* Cuenta Admin Site: joselu20tbr/joselu20tbr

## Resumen parte obligatoria
Esta práctica consiste en salas de chat completamente abiertas entre usuarios.\
* Los nombres anónimos son nombres de Pokemon de forma aleatoria. Dicho nombre se puede cambiar en el menú de configuración.
* Como errata, la página se llama DeCharlas, empecé así y así lo dejé hasta el final, pero no afecta a la funcionalidad.
* Las contraseñas están relacionadas con mis grupos de amigos, no tienen la intención de ser ofensivas.
* Para asegurar la funcionalidad, se debe descargar la biblioteca pypokedex mediante: pip install pypokedex.

## Lista partes opcionales
* Nombre parte: favicon.ico
* Nombre parte: permitir logout
* Nombre parte: votación por sala
