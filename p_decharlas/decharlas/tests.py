from django.test import TestCase
from django.test import Client
from .models import Password, User, Message

from http.cookies import SimpleCookie



class GetTests (TestCase):


    def test_nocookie(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 302)


    def test_login(self):
        c = Client()
        response = c.get('/login')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<p>LOGIN DECHARLAS</p>', content)


    def test_root(self):
        c = Client()
        Password.objects.create(valid_pwd='km21')
        response = c.get('/login')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<p>LOGIN DECHARLAS</p>', content)

        response = c.post('/login', {'login_pwd': 'km21'})
        self.assertEqual(response.status_code, 302)

        self.client.cookies = SimpleCookie({'userID': User.objects.filter(id=1)})
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<title>DeCharlas</title>', content)


    def test_config(self):
        c = Client()
        Password.objects.create(valid_pwd='km21')
        response = c.get('/login')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<p>LOGIN DECHARLAS</p>', content)

        response = c.post('/login', {'login_pwd': 'km21'})
        self.assertEqual(response.status_code, 302)

        self.client.cookies = SimpleCookie({'userID': User.objects.filter(id=1)})
        response = c.get('/config')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<label for="config_name">Name</label>', content)


    def test_help(self):
        c = Client()
        Password.objects.create(valid_pwd='km21')
        response = c.get('/login')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<p>LOGIN DECHARLAS</p>', content)

        response = c.post('/login', {'login_pwd': 'km21'})
        self.assertEqual(response.status_code, 302)

        self.client.cookies = SimpleCookie({'userID': User.objects.filter(id=1)})
        response = c.get('/help')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        # self.assertIn('<label for="config_name">Name</label>', content)


    def test_room(self):
        c = Client()
        Password.objects.create(valid_pwd='km21')
        response = c.get('/login')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<p>LOGIN DECHARLAS</p>', content)

        response = c.post('/login', {'login_pwd': 'km21'})
        self.assertEqual(response.status_code, 302)

        self.client.cookies = SimpleCookie({'userID': User.objects.filter(id=1)})

        response = c.post('/', {'roomname': 'test_room'})
        self.assertEqual(response.status_code, 302)

        response = c.get('/test_room')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<label for="content">Enter Message</label>', content)


    def test_dynroom(self):
        c = Client()
        Password.objects.create(valid_pwd='km21')
        response = c.get('/login')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<p>LOGIN DECHARLAS</p>', content)

        response = c.post('/login', {'login_pwd': 'km21'})
        self.assertEqual(response.status_code, 302)

        self.client.cookies = SimpleCookie({'userID': User.objects.filter(id=1)})

        response = c.post('/', {'roomname': 'test_room'})
        self.assertEqual(response.status_code, 302)

        response = c.get('/dyn/test_room')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<p>DYNAMIC ROOM</p>', content)


    def test_logout(self):
        c = Client()
        Password.objects.create(valid_pwd='km21')
        response = c.get('/login')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<p>LOGIN DECHARLAS</p>', content)

        response = c.post('/login', {'login_pwd': 'km21'})
        self.assertEqual(response.status_code, 302)

        self.client.cookies = SimpleCookie({'userID': User.objects.filter(id=1)})

        response = c.get('/logout')
        self.assertEqual(response.status_code, 302)



