from django.http import HttpResponse, JsonResponse, Http404, HttpResponseNotAllowed
from .models import Message, Room, User, Password, Room_Register, Room_Vote
from django.template import loader
from django.shortcuts import redirect
from datetime import datetime, timedelta
from django.views.decorators.csrf import csrf_exempt
from xml.dom.minidom import parseString
import pypokedex
import random
from django.utils import timezone
from .forms import Font_Form


class Stats:

    def __init__(self):
        self.text = Message.objects.filter(isimg=False).count()
        self.img = Message.objects.filter(isimg=True).count()
        self.rooms = Room.objects.all().count()

#
# MAIN METHODS
#
def login(request):

    if request.method == 'GET' or request.method == 'POST':

        if request.method == 'POST':
        # If the method is POST, it comes from the form, and here it is decided to admit the user or not
            input_pwd = request.POST['login_pwd']

            try:
                Password.objects.get(valid_pwd=input_pwd)

                name = pypokedex.get(dex=random.randint(1, 1010)).name
                userID = random.randint(1, 10 ** 10)
                new_user = User(name=name, userID=userID)
                new_user.save()

                final_res = redirect('/')
                expire_date = cookie_date()
                final_res.set_cookie("userID", value=userID, expires=expire_date)

                return final_res

            except Password.DoesNotExist:
                context = {
                    'failed_post': True,
                }

        if request.method == 'GET':
            context = {
                'failed_post': False,
            }

        template = loader.get_template('decharlas/login.html')
        return HttpResponse(template.render(context, request))

    else:
         return HttpResponse('Page not exist')



def logout(request):
    final_res = redirect('/login')
    final_res.delete_cookie('userID')
    return final_res


def general(request):
    # GET method retreives the main page
    # POST method comes from the user form on the right

    if request.method == 'GET' or request.method == 'POST':

        if 'userID' in request.COOKIES:
            user_ID = request.COOKIES['userID']
        else:
            return redirect('/login')

        try:
           user_info = User.objects.get(userID=user_ID)
        except User.DoesNotExist:
            return logout(request)

        if request.method =="GET":

            template = loader.get_template('decharlas/general.html')
            room_list = compose_general_room_list
            main_stats = Stats()
            context = {
                'user_info': user_info,
                'room_list': room_list,
                'room_unread': room_unread(user_info),
                'like_list': like_list(),
                'dislike_list': dislike_list(),
                'total_msg': room_total_msg(),
                'stats': main_stats,
            }

            final_res = HttpResponse(template.render(context, request))

        else:
            final_res = newroom(request)

        return final_res

    else:
        return HttpResponseNotAllowed(['POST', 'GET'])


def config(request):
    # If method is GET, return the config page
    # If method is POST, change the configuration and redirect to main page

    if request.method == 'GET' or request.method == 'POST':

        if 'userID' in request.COOKIES:
            user_ID = request.COOKIES['userID']
        else:
            return redirect('/login')

        try:
            user_info = User.objects.get(userID=user_ID)
        except User.DoesNotExist:
            return logout(request)

        if request.method == "GET":

            template = loader.get_template('decharlas/config.html')
            form = Font_Form
            main_stats = Stats()
            context = {
                'user_info': user_info,
                'form': form,
                'stats': main_stats,
            }

            final_res = HttpResponse(template.render(context, request))

        else:
            action = request.POST.get('action')

            if action == "newroom":
                return newroom(request)

            font_type = request.POST['font_type']
            font_size = request.POST['font_size']
            new_name = request.POST['config_name']

            if not new_name == "":
                user_info.name = new_name

            user_info.font_type = font_type
            user_info.font_size = font_size
            user_info.save()

            final_res = redirect('/')

        return final_res

    else:

        return HttpResponseNotAllowed(['POST', 'GET'])


def help(request):

    if request.method == 'GET' or request.method == 'POST':

        if 'userID' in request.COOKIES:
            user_ID = request.COOKIES['userID']
        else:
            return redirect('/login')

        try:
            user_info = User.objects.get(userID=user_ID)
        except User.DoesNotExist:
            return logout(request)

        if request.method == "GET":

            template = loader.get_template('decharlas/help.html')
            main_stats = Stats()
            context = {
                'user_info': user_info,
                'stats': main_stats,
            }

            final_res = HttpResponse(template.render(context, request))

        else:
            return newroom(request)

        return final_res

    else:
        return HttpResponseNotAllowed(['POST', 'GET'])


@csrf_exempt
def rsc(request, resource):
    # Both GET and POST methods retrieves the same page, but POST inserts a new message
    # POST can be received from two different posts, from the message form and the room nav form
    if request.method == "GET" or request.method == "POST" or request.method == "PUT":

        if request.method == "PUT":
            if 'userID' in request.COOKIES:
                user_ID = request.COOKIES['userID']
            else:
                if 'Authorization' in request.headers:
                    access_pwd = request.headers['Authorization']
                    try:
                        Password.objects.get(valid_pwd=access_pwd)

                        name = "Anonymous"
                        user_ID = random.randint(1, 10 ** 10)
                        new_user = User(name=name, userID=user_ID)
                        new_user.save()

                    except Password.DoesNotExist:
                        print("Password not found")
                        return HttpResponse('Need an Authorization header with a valid password', status=401)

                else:
                    return HttpResponse('Need an Authorization header with a valid password', status=401)

            user_info = User.objects.get(userID=user_ID)
            return xml_update(request, resource, user_info)

        if request.method == "GET" or request.method == "POST":

            if 'userID' in request.COOKIES:
                user_ID = request.COOKIES['userID']
            else:
                return redirect('/login')

            try:
                user_info = User.objects.get(userID=user_ID)
            except User.DoesNotExist:
                return logout(request)

            room_info = Room.objects.get(name=resource)

            if request.method == "POST":

                action = request.POST.get('action')

                if action == "newroom":
                    return newroom(request)

                elif action == 'like' or action == 'dislike':
                    update_vote(request, user_info, room_info)
                    return redirect('/' + room_info.name)

                else:

                    newmsg(request, room_info, user_info)

            user_register(room_info, user_info)

            msg_list = Message.objects.filter(room=room_info).order_by("-date")

            template = loader.get_template('decharlas/room.html')
            main_stats = Stats()

            context = {
                'user_info': user_info,
                'room_info': room_info,
                'msg_list': msg_list,
                'stats': main_stats,
            }

            final_res = HttpResponse(template.render(context, request))
            return final_res

    else:
        return HttpResponseNotAllowed(['POST', 'GET', 'PUT'])

def jsonrsc(request, resource):

    if request.method == "GET":

        if not 'userID' in request.COOKIES:
            return redirect('/login')

        room_info = Room.objects.get(name=resource)
        msg_list = Message.objects.filter(room=room_info).order_by("-date")

        msg_dict_list = []

        for msg in msg_list:
            msg_dict = {
                "author": str(msg.author.name),
                "text": msg.content,
                "isimg": msg.isimg,
                "date": msg.date,
            }
            msg_dict_list.append(msg_dict)

        json_list = JsonResponse(msg_dict_list, safe=False)
        return json_list

    else:
        return HttpResponseNotAllowed(['GET'])


def dynrsc(request, resource):

    if request.method == "GET" or request.method == "POST":

        if 'userID' in request.COOKIES:

            user_ID = request.COOKIES['userID']

        else:
            return redirect('/login')

        try:
            user_info = User.objects.get(userID=user_ID)
        except User.DoesNotExist:
            return logout(request)

        room_info = Room.objects.get(name=resource)


        if request.method == "POST":

            action = request.POST.get('action')

            if action == "newroom":
                return newroom(request)

            elif action == 'like' or action == 'dislike':
                update_vote(request, user_info, room_info)
                return redirect('/' + room_info.name)

            else:

                newmsg(request, room_info, user_info)

        user_register(room_info, user_info)
        template = loader.get_template('decharlas/dynroom.html')
        main_stats = Stats()
        context = {
            'user_info': user_info,
            'room_info': room_info,
            'stats': main_stats,
        }

        final_res = HttpResponse(template.render(context, request))
        return final_res

    else:
        return HttpResponseNotAllowed(['POST', 'GET'])



#
# SIDE METHODS
#
def cookie_date():
    return datetime.now() + timedelta(days=730)

def newroom(request):

    room_name = request.POST["roomname"].rstrip()
    # The rstrip is used to remove the blank spaces at the end of the room name

    if room_name == "":
        return redirect('/')

    try:
        room = Room.objects.get(name=room_name)

    except Room.DoesNotExist:
        # If the room doesn't exist, it needs to be created
        new_room = Room(name=room_name)
        new_room.save()

        # The creator is a foreign key, so it need to me added
        room = Room.objects.get(name=room_name)

    final_res = redirect('/' + room.name)
    return final_res


def room_unread(user_info):

    room_list = Room.objects.all()
    unread_list = {}

    for room in room_list:

        try:
            user_register = Room_Register.objects.get(user=user_info, room=room)
            msg_count = 0
            msg_list = Message.objects.filter(room=room)

            if not msg_list:
                unread_list.update({room.name: "-1"})

            else:
                for message in msg_list:
                    if message.date >= user_register.date and message.author != user_info:
                        msg_count += 1

                unread_list.update({room.name: str(msg_count)})


        except Room_Register.DoesNotExist:
            unread_list.update({room.name: "-1"})

    return unread_list



def room_total_msg():

    room_list = Room.objects.all()
    total_list ={}

    for room in room_list:
        room_total = Message.objects.filter(room=room).count()
        total_list.update({room.name: str(room_total)})

    return total_list


def like_list():

    room_list = Room.objects.all()
    like_list = {}

    for room in room_list:
        like_vote = Room_Vote.objects.filter(room=room, vote=True).count()
        like_list.update({room.name: str(like_vote)})

    return like_list

def dislike_list():

    room_list = Room.objects.all()
    dislike_list = {}

    for room in room_list:
        dislike_vote = Room_Vote.objects.filter(room=room, vote=False).count()
        dislike_list.update({room.name: str(dislike_vote)})

    return dislike_list



def update_vote(request, user_info, room_info):

    action = request.POST.get('action')
    action_vote = action == 'like'

    try:
        user_vote = Room_Vote.objects.get(room=room_info, user=user_info)

        if user_vote.vote != action_vote:
            user_vote.vote = action_vote
            user_vote.save()

    except Room_Vote.DoesNotExist:
        user_vote = Room_Vote(room=room_info, user=user_info, vote=action_vote)
        user_vote.save()


def compose_general_room_list():

    room_list = Room.objects.all()
    room_show_list = []

    for room in room_list:
        if Message.objects.filter(room=room).count() != 0:
            room_show_list.append(room)

    return room_show_list


def newmsg(request, room, user):

    msg_content = request.POST['content']
    msg_isimg = request.POST.get('isimg', False)
    if msg_isimg == 'on':
        msg_isimg = True
    else:
        msg_isimg = False
    msg_date = timezone.now()

    new_msg = Message(content=msg_content,
                      date=msg_date,
                      isimg=msg_isimg,
                      author=user,
                      room=room)
    new_msg.save()


def user_register(room, user):

    try:
        user_register = Room_Register.objects.get(user=user, room=room)
        user_register.date = timezone.now()
        user_register.save()

    except Room_Register.DoesNotExist:
        user_register = Room_Register(user=user,
                                      room=room,
                                      date=timezone.now())
        user_register.save()


def xml_update(request, resource, user):

    xml_doc = parseString(request.body)

    try:
        room = Room.objects.get(name=resource)
    except Room.DoesNotExist:
        return HttpResponse('Room not exists', status=404)

    msg_list = xml_doc.getElementsByTagName('message')

    for msg in msg_list:
        isimg = msg.getAttribute('isimg')

        if isimg == "True":
            msg_isimg = True
        else:
            msg_isimg = False

        text = msg.getElementsByTagName('text')
        content = text[0].firstChild.nodeValue
        date = timezone.now()

        message = Message(content=content,
                          date=date,
                          isimg=msg_isimg,
                          author=user,
                          room=room)
        message.save()

    final_res = HttpResponse(status=200)
    return final_res

